package com.manoel.Inicio;

import com.manoel.Dados.ClasseTeste;
import com.manoel.Dados.Teste;

public class PassagemDeValoresEstaticos {
    public static void main(String[] args) {
        ClasseTeste.campo = "teste aqui";
        ClasseTeste.ExibeCampo();
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                TesteDaVariavelCampo(true, i);
            } else {
                TesteDaVariavelCampo(false, i);
            }
        }
        Teste teste = new Teste();
        System.out.println(teste.getEstado());
    }

    public static void TesteDaVariavelCampo(boolean altera, int contador) {
        if (altera) {
            ClasseTeste.campo = "foi alterado no " + contador;
        } else {
            ClasseTeste.campo = "não foi alterado no " + contador;
        }
    }
}
