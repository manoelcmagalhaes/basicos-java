package com.manoel.Inicio;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LeituraArquivo {

    static String pastaBase = "D:\\bunker\\";

    public static void main(String[] args) {
        //Criação
        String ark1 = CriaArquivoComFile("arquivo1.txt");
        String ark2 = CriaArquivoComNIO("arquivo2.txt");
        //Escrita no arquivo 1
        EscreveComBufferedWriter("teste com buffered writer", ark1);
        EscreveComFileOutputStream("teste com file output stream", ark1);
        EscreveComFileWriter("teste com file writer", ark1);
        EscreveComPrintWriter("teste com print writer", ark1);
        //Escrita no arquivo 2
        EscreveComBufferedWriter("teste com buffered writer", ark2);
        EscreveComFileOutputStream("teste com file output stream", ark2);
        EscreveComFileWriter("teste com file writer", ark2);
        EscreveComPrintWriter("teste com print writer", ark1);
        //Leitura do arquivo 1
        LeituraComScanner(ark1);
        LeituraComLines(ark1);
        LeituraComBufferedReader(ark1);
        //Leitura do arquivo 2
        LeituraComScanner(ark2);
        LeituraComLines(ark2);
        LeituraComBufferedReader(ark2);
    }

    public static String CriaArquivoComFile(String nome) {
        try {
            File arquivo = new File(pastaBase + nome);
            if (arquivo.createNewFile()) {
                return arquivo.getAbsolutePath();
            } else {
                return arquivo.getAbsolutePath();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "erro";
        }
    }

    public static String CriaArquivoComNIO(String nome) {
        try {
            Path arquivo = Paths.get(pastaBase + nome);
            Files.createFile(arquivo);
            return arquivo.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "erro";
        }
    }

    public static void EscreveComFileWriter(String texto, String arquivo) {
        try {
            FileWriter escritor = new FileWriter(arquivo);
            escritor.write(texto);
            escritor.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void EscreveComBufferedWriter(String texto, String arquivo) {
        try {
            BufferedWriter escritor = new BufferedWriter(new FileWriter(arquivo));
            escritor.write(texto);
            //escritor.append(texto); //append
            escritor.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void EscreveComPrintWriter(String texto, String arquivo) {
        try {
            FileWriter escritor = new FileWriter(arquivo);
            PrintWriter printer = new PrintWriter(escritor);
            printer.print(texto);
            printer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void EscreveComFileOutputStream(String texto, String arquivo) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(arquivo);
            byte[] conteudo = texto.getBytes(StandardCharsets.UTF_8);
            fileOutputStream.write(conteudo);
            fileOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void LeituraComScanner(String url) {
        File arquivo = new File(url);
        try {
            Scanner leitor = new Scanner(arquivo);
            while (leitor.hasNext()) {
                System.out.println(leitor.nextLine());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void LeituraComBufferedReader(String url) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(url));
            String linha = bufferedReader.readLine();
            while (Objects.nonNull(linha)) {
                System.out.println(linha);
                linha = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void LeituraComLines(String url) {
        try {
            Path path = Paths.get(LeituraArquivo.class.getClassLoader().getResource(url).toURI());
            Stream<String> linhas = Files.lines(path);
            String dados = linhas.collect(Collectors.joining("\n"));
            linhas.close();
            System.out.println(dados);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
